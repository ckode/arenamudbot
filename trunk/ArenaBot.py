#  ArenaMUD2 - A multiplayer combat game - http://arenamud.david-c-brown.com
#  Copyright (C) 2012 - David C Brown & Mark Richardson
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from twisted.internet import reactor
from twisted.internet.protocol import ClientFactory
from twisted.conch.telnet import TelnetTransport

from ambot.classes import Barbarian
from ambot.races import Ogre
from ambot.ambot import amBot

import sys

# All usable class and races must exist here.
CLASSES = { "barbarian": Barbarian }
RACES = { "ogre": Ogre }
NAMES = ["Botman", "Eddiefingers", "Wallbanger"]



class BotFactory(ClientFactory):
   """
   Bot's Client Factory class
   """

   def __init__(self):
      """
      Initialize Bot Factory class object
      """
      pass


def StartUp():

   factory = BotFactory()
   factory.protocol = lambda: TelnetTransport(amBot, )
   reactor.connectTCP('127.0.0.1', 23, factory)
   reactor.run()
   

def displayHelp():
   """
   Display command line help.
   """

   print( "Supported classes are: ")
   for option in CLASSES.keys():
      print(" * {0}".format(option))

   print("")
   print( "Supported races are: ")
   for option in RACES.keys():
      print(" * {0}".format(option))

   print("")
   print("Example: 'python {0} barbarian ogre Jack'".format(sys.argv[0]) )
   sys.exit(0)

if __name__ == "__main__":

   if len(sys.argv) == 2:
      if sys.argv[1].lower() == "help":
          displayHelp()

   if len(sys.argv) is not 3 or len(sys.argv) is not 4:
      print("ArenaMUD bot required perameters not correct.")
      print("Example:  python {0} <class> <race> <optional name>".format(sys.argv[0]))
      print("Or 'python {0} help' for help.".format(sys.argv[0]))
      sys.exit(1)
   else:
      if len(sys.argv) == 3:
         x = range(0, len(NAMES))
         name = NAMES[x]
         StartUp(sys.argv[1], sys.argv[2], name)
      else:
         StartUp(sys.argv[1], sys.argv[2], sys.argv[3])

