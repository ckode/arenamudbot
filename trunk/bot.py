from twisted.internet import reactor
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.conch.telnet import TelnetTransport, StatefulTelnetProtocol

import string, time

class ArenaMUDBot(StatefulTelnetProtocol):

   def __init__(self):
       self.setLineMode()
       self.statline = True
       self.spawn = False
       self.inCombat = False
       self.slamtime = True

       self.ansiCodes = [chr(27) + "[0m", chr(27) + "[1m", chr(27) + "[3m", chr(27) + "[4m", chr(27) + "[7m", chr(27) + "[9m",
                          chr(27) + "[22m", chr(27) + "[23m", chr(27) + "[24m", chr(27) + "[27m", chr(27) + "[29m", chr(27) + "[30m",
                          chr(27) + "[31m", chr(27) + "[32m", chr(27) + "[33m", chr(27) + "[1;33m", chr(27) + "[34m", chr(27) + "[35m",
                          chr(27) + "[36m", chr(27) + "[37m", chr(27) + "[39m", chr(27) + "[1;31m", chr(27) + "[1;32m", chr(27) + "[1;34m",
                          chr(27) + "[1;35m", chr(27) + "[1;36m", chr(27) + "[40m", chr(27) + "[41m", chr(27) + "[42m", chr(27) + "[43m",
                          chr(27) + "[44m", chr(27) + "[45m", chr(27) + "[46m", chr(27) + "[47m", chr(27) + "[49m", chr(27) + "[2K",
                          chr(27) + "[80D", chr(27) + "6n", chr(27) + "[1K", chr(27) + "[2J", chr(27) + "s", chr(27) + "u"]



   def connectionMade(self):
       #self.sendLine("Bot")
       #self.sendLine("What a fine day it is.")
       pass

   def lineReceived(self, line):
       line = self.cleanLine(line)

       print line
       #if "Enter your login or type 'new':" in line:
       #    self.sendLine("Bot")
       if "[HP=" in line and self.spawn == True:
          self.statline = False
          time.sleep(1)
          self.printsend("statline off")


       if "has killed Bot!" in line:
          self.spawn = True
          time.sleep(1)
          killer = line.split(line)[0]
          print line.split()
          print killer + " killed me"
          self.printsend("gos Fuck you {0}, you asshole!  You're fucking dead!".format(line.split()[0]))
          time.sleep(1)
          self.printsend("spawn")

       if "ArenaMUD Battleground (C)2011 SonzoSoft Software" in line:
          self.printsend("Bot")
       if "Choose a class" in line:
          self.printsend("1")
       if "Choose a race" in line:
          self.printsend("3")

       if "Type 'spawn' to spawn, type 'help' for help" in line and self.spawn == False:
          self.spawn = True
          time.sleep(1)
          self.printsend("spawn")
          reactor.callLater(1, self.hasSpawn)

       if "you for" in line or "at you" in line and self.inCombat is False:
          enemy = line.split()[0]
          self.printsend("slam " + enemy)
          self.printsend("attack " + enemy)

       if "moves to attack you!" in line and self.inCombat == False:
          attacker = line.split()[0]
          self.printsend("slam {0}".format(attacker))
          self.printsend("attack {0}".format(attacker))
          
       if "casts" in line and "you" in line and self.inCombat == False:
          attacker = line.split()[0]
          self.printsend("slam {0}".format(attacker))
          self.printsend("attack {0}".format(attacker))

       if "*Combat Engaged*" in line:
          self.isCombat = True
          print "In combat!"

       if "*Combat Off*" in line:
          self.inCombat = False
          print "Combat ended!"
       if "Whos Online" in line:
          self.printsend("spawn")

   def hasSpawn(self):
       self.spawn = False

   def cleanLine(self, line):
       for code in self.ansiCodes:
           line = line.replace(code, "")
      #Delete characters before backspaces
       pos = 0
       lineSize = len(line)
       newline = ""
       for character in line:
           if character == chr(0x08):
               newline = newline[:-1]
           else:
               newline += character
      # Remove all unprintable characters
       line = filter(lambda x: x in string.printable, newline)

       return line

   def printsend(self, text):
       print "Bot said: " + text
       self.sendLine(text)

class BotFactory(ClientFactory):
    def __init__(self):
       pass

    #def builtProtocol(self, "127.0.0.1"):
    #   return TelnetTransfport(ArenaMUDBot)

factory = BotFactory()
factory.protocol = lambda: TelnetTransport(ArenaMUDBot)
reactor.connectTCP('127.0.0.1', 23, factory)
reactor.run()